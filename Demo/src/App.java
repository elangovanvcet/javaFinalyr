//import java.util.Scanner;

public class App {
    static int o=9;
    public static void main(String[] args) {

        //TODO : BASICS

       System.out.println(o);

        //to comment and uncomment line--->ctrl+/
        //! dataype -> primitive datatype--> byte , short, int, float, double, long , boolean , char
         //!  :Non primitive datatypes--->String , arrays, list etc.. 
        //className , string class-->each 1st word letter caps
        //functions,variables, data type--->camel case
        //class package always small
        // Scanner userData =new Scanner(System.in);
        // String name=new String();
        // name= userData.next();//returns a string
        // System.out.println(name);
        // name= userData.nextLine();//whole line
        // System.out.println(name);
        // int a=6;
        // float b=5;//implicit conversion--->whenever the right side value is smaller than left side value , its automatically CONVERTED
        // System.out.println(b);
        // int c=(int)6.9;//explicit conversion--->whenever the right side value is larger than left side value , we have to CONVERT manually        System.out.println(b);
        // System.out.println(c);
        
        // float d=5.7f;
        // char e='r';
        // System.out.println(e);
        // boolean vasanthIsCommited=true;
        // byte f=127;//   -128 to 127
        // short g=10000;
        // long u=345463453434436499l;
        // System.out.println(u);

        
        // String firstName="Narmu";
        // System.out.println(firstName);
        // System.out.println( firstName.length());
        // System.out.println( firstName.charAt(4));
        // System.out.println( firstName.substring(0, firstName.length()));//ar


       //TODO:CLASSES

       //! classname objectname=new classNameConstructor

       Demo d=new Demo();
       System.out.println(d.a);
       System.out.println(Demo.b);
       Demo d1=new Demo();
       System.out.println(d.a);
       d1.refer();
       System.out.println(Demo.cube(7));
    }
}
